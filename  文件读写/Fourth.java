import java.io.*;
public class Fourth {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("F:\\java\\yunbankezuoyeceshi", "First.txt");
        //File file = new File("HelloWorld.txt");
//      File file1 = new File("C:\\Users\\besti\\Desktop\\FileTest\\Test\\Test");
//      file1.mkdir();
//      file1.mkdirs();
        if (!file.exists()) {
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] hello = {'H','e','l','l','o',',','W','o','r','l','d','!'};
        outputStream1.write(hello);
        outputStream1.flush();//可有可无，不执行任何操作！！！
        byte[] buffer = new byte[1024];
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
        System.out.println("文件读结束：BufferedInputStream直接读并输出！");

        //====================================BufferedOutputstream================================================
        OutputStream outputStream2 = new FileOutputStream(file);
        BufferedOutputStream bufferedOutputStream2 = new BufferedOutputStream(outputStream2);
        String content2 = "利用BufferedOutputStream写入文件的缓冲区内容";
        bufferedOutputStream2.write(content2.getBytes(),0,content2.getBytes().length);
        bufferedOutputStream2.flush();
        bufferedOutputStream2.close();

    }
}

