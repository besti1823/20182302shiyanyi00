import junit.framework.TestCase;
import static org.junit.Assert.assertNotEquals;
public class Sorting2Test extends TestCase {
    public void testselectionSort() {
        String expect = "[2, 18, 20, 23]";
        String expect2 = "[1, 2, 3, 4, 5]";
        String expect3 = "[2, 3, 4, 1]";
        String expect4 = "[1, 2, 3, 4]";
        int test1[] = {20,18,23,2,};
        int test2[] = {1,2,3,4,5};
        int test3[] = {5,4,3,2,1};
        int test4[] = {2, 3, 1, 4};
        int test5[] = {1, 2, 3, 4};
        int test6[] = {4, 3, 2, 1};
        assertEquals(expect,Sorting2.selectionSort(test1));
        assertNotEquals(expect2,Sorting2.selectionSort(test1));
        assertEquals(expect2,Sorting2.selectionSort(test2));
        assertNotEquals(expect,Sorting2.selectionSort(test2));
        assertEquals(expect2,Sorting2.selectionSort(test3));
        assertNotEquals(expect,Sorting2.selectionSort(test3));
        assertEquals(expect4,Sorting2.selectionSort(test4));
        assertNotEquals(expect3,Sorting2.selectionSort(test4));
        assertEquals(expect4,Sorting2.selectionSort(test5));
        assertNotEquals(expect3,Sorting2.selectionSort(test5));
        assertEquals(expect4,Sorting2.selectionSort(test6));
        assertNotEquals(expect3,Sorting2.selectionSort(test6));
    }
}
