public class Sorting5 {
    public String maopao(String s)
    {
        String[] a = s.split("\\s");
        int[] b = new int[a.length];
        for(int i=0;i<b.length;i++)
        {
            int num = Integer.parseInt(a[i]);
            b[i]=num;
        }
        for(int i=0;i<b.length-1;i++)
        {
            for(int j=0;j<b.length-i-1;j++)
            {
                if(b[j]>b[j+1])
                {
                    int t ;
                    t=b[j];
                    b[j]=b[j+1];
                    b[j+1]=t;
                }
            }
        }
        String result = "";
        for(int i=0;i<b.length;i++)
        {
            result += " "+b[i];
        }
        return  result;
    }
    public String selectionsort(String a)
    {
        String[] s = a.split("\\s");
        int[] b = new int[s.length];
        for(int i=0;i<b.length;i++)
        {
            int num = Integer.parseInt(s[i]);
            b[i]=num;
        }
        for(int i=0;i<b.length-1;i++)
        {
            for(int j=i+1;j<b.length;j++)
            {
                if(b[i]<b[j])
                {
                    int t = b[i];
                    b[i]=b[j];
                    b[j]=t;
                }
            }
        }
        String result="";
        for(int i=0;i<b.length;i++)
        {
            result+=" "+b[i];
        }
        return  result;
    }
    public String   sorting (int[] arr)
    {
        for(int gap = arr.length/2;gap>0;gap/=2)
        {
            for(int i=gap;i<arr.length;i++)
            {
                int j =i;
                while (j-gap>=0&&arr[j]<arr[j-gap])
                {
                    int t = arr[j];
                    arr[j]=arr[j-gap];
                    arr[j-gap]=t;
                    j-=gap;
                }
            }
        }
        String result = "";
        for(int i=0;i<arr.length;i++)
        {
            result += " "+arr[i];
        }
        return result;
    }
    public static void heapify(int[] arrays, int currentRootNode, int size) {
        if (currentRootNode < size) {
            int left = 2 * currentRootNode + 1;
            int right = 2 * currentRootNode + 2;
            int max = currentRootNode;

            if (left < size) {

                if (arrays[max] < arrays[left]) {
                    max = left;
                }
            }
            if (right < size) {

                if (arrays[max] < arrays[right]) {
                    max = right;
                }
            }
            if (max != currentRootNode) {
                int temp = arrays[max];
                arrays[max] = arrays[currentRootNode];
                arrays[currentRootNode] = temp;
                heapify(arrays, max, size);
            }
        }
    }
    public static void maxHeapify(int[] arrays, int size) {
        for (int i = size - 1; i >= 0; i--) {
            heapify(arrays, i, size);
        }

    }
    public static String rudui(int[] arrays)
    {
        for (int i = 0; i < arrays.length; i++) {
            maxHeapify(arrays, arrays.length - i);
            int temp = arrays[0];
            arrays[0] = arrays[(arrays.length - 1) - i];
            arrays[(arrays.length - 1) - i] = temp;
        }
        String result="";
        for(int i=0;i<arrays.length;i++)
        {
            result =result+ ""+arrays[i];
        }
        return  result;
    }
}