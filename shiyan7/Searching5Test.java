import junit.framework.TestCase;
public class Searching5Test extends TestCase {
    public void testLinearSearch()
    {
        double test[] = {1, 7, 11, 9, 23};
        assertEquals(true, Searching5.linearSearch(test,  1));
        assertEquals(true, Searching5.linearSearch(test,  23));
        assertEquals(true, Searching5.linearSearch(test,  9));
        assertEquals(false, Searching5.linearSearch(test,  100));
        double test2[] = {1, 2, 3, 4, 5};
        assertEquals(true, Searching5.linearSearch(test2,  1));
        assertEquals(true, Searching5.linearSearch(test2,  5));
        assertEquals(true, Searching5.linearSearch(test2,  3));
        assertEquals(false, Searching5.linearSearch(test2,  100));
        double test3[] = {5, 4, 3, 2, 1};
        assertEquals(true, Searching5.linearSearch(test3,  1));
        assertEquals(true, Searching5.linearSearch(test3,  5));
        assertEquals(true, Searching5.linearSearch(test3,  3));
        assertEquals(false, Searching5.linearSearch(test3,  100));
        double test4[] = {2, 0, 1, 8, 2, 3, 0, 2};
        assertEquals(true, Searching5.linearSearch(test4,  0));
        assertEquals(true, Searching5.linearSearch(test4, 0));
        assertEquals(true, Searching5.linearSearch(test4,  8));
        assertEquals(false, Searching5.linearSearch(test4,  100));
        double ARR5[] = {1, 2, 3, 2};
        assertEquals(true, Searching5.linearSearch(ARR5, 1));
        assertEquals(true, Searching5.linearSearch(ARR5, 2));
        assertEquals(true, Searching5.linearSearch(ARR5, 2));
        assertEquals(false, Searching5.linearSearch(ARR5,  100));
        double test6[] = {2, 3, 2, 1};
        assertEquals(true, Searching5.linearSearch(test6, 2));
        assertEquals(true, Searching5.linearSearch(test6,  1));
        assertEquals(true, Searching5.linearSearch(test6, 3));
        assertEquals(false, Searching5.linearSearch(test6,  100));
    }
    }