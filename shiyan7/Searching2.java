public class Searching2 {
    public static <T extends Comparable<? super T>>boolean linearSearch(T[] data, int min,int max,T target){
        int index =min;
        boolean found =true;
        while (!found && index<=max){
            if (data[index].compareTo(target)==0)
                found=true;
            index++;
        }
        return found;
    }
}
