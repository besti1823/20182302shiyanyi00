package test;
import cn.edu.besti.cs1823.S2302.Sorting1;
import junit.framework.TestCase;
public class SortingTest extends TestCase {
    public void testselectionSort() {
        String expect = "[2, 18, 20, 23]";
        String expect2 = "[1, 2, 3, 4, 5]";

        String expect3 = "[2, 3, 20, 0]";
        String expect4 = "[0, 2, 3, 20]";


        //正常
        int test[] = {20,18,23,02,};
        assertEquals(expect,Sorting1.selectionSort(test));
        assertNotEquals(expect2,Sorting1.selectionSort(test));
        //正序
        int test2[] = {1,2,3,4,5};
        assertEquals(expect2,Sorting1.selectionSort(test2));
        assertNotEquals(expect,Sorting1.selectionSort(test2));
        //倒序
        int test3[] = {5,4,3,2,1};
        assertEquals(expect2,Sorting1.selectionSort(test3));
        assertNotEquals(expect,Sorting1.selectionSort(test3));
        //正常
        int test4[] = {2, 3, 0, 20};
        assertEquals(expect4,Sorting1.selectionSort(test4));
        assertNotEquals(expect3,Sorting1.selectionSort(test4));
        //正序
        int test5[] = {0, 2, 3, 20};
        assertEquals(expect4,Sorting1.selectionSort(test5));
        assertNotEquals(expect3,Sorting1.selectionSort(test5));
        //正序
        int test6[] = {20, 3, 2, 0};
        assertEquals(expect4,Sorting1.selectionSort(test6));
        assertNotEquals(expect3,Sorting1.selectionSort(test6));

    }

    private void assertNotEquals(String expect2, String selectionSort) {
    }


}