package zuoye10;

import java.util.Scanner;
public class Directed_MGraph {
    public static void main(String[] args) {
        MGraph G = directed_mgraph();
        printResult(G.vexs,G.edgs);
    }
    public static MGraph directed_mgraph() {
        System.out.println("请输入顶点的个数：");
        Scanner sc = new Scanner(System.in);
        int _numNodes = sc.nextInt();
        String[] vexs = new String[_numNodes];
        int[][] edgs = new int[_numNodes][_numNodes];
        System.out.println("请输入顶点名称：");
        for(int i=0;i<vexs.length;i++) {
            sc = new Scanner(System.in);
            vexs[i] = sc.nextLine();
        }
        for(int i=0;i<_numNodes;i++) {
            for(int j=0;j<_numNodes;j++) {
                double random = Math.random()*10;
                if(i==j)
                    edgs[i][j] = 0;
                else if(random<2)
                    edgs[i][j] = 0;
                else
                    edgs[i][j] = (int)random;
            }
        }
        MGraph G = new MGraph();
        G.vexs = vexs;
        G.edgs = edgs;
        return G;
    }
    public static void printResult(String[] vexs,int[][] edgs) {
        System.out.println("顶点的数组：");
        System.out.print("[");
        for (int i = 0; i < vexs.length; i++) {
            if (i == vexs.length - 1) {
                System.out.print(vexs[i]);
                break;
            } else
                System.out.print(vexs[i] + ",");
        }
        System.out.println
                ("]");
        System.out.println("边的数组");
        System.out.print("  ");
        for (int i = 0; i < vexs.length; i++) {
            if (i == vexs.length - 1) {
                System.out.print(vexs[i]);
                break;
            } else
                System.out.print(vexs[i] + ",");
        }
        System.out.println();
        for (int i = 0; i < vexs.length; i++) {
            System.out.print(vexs[i] + "|");
            for (int j = 0; j < vexs.length; j++) {
                if (j == vexs.length - 1) {
                    System.out.print(edgs[i][j]);
                    break;
                } else
                    System.out.print(edgs[i][j] + ",");
            }
            System.out.println();
        }
    }
}
