package zuoye10;

import java.util.Scanner;
public class ALGraph {
    class Vexs{
        String data;
        Chain chain;
    }
    class Chain{
        int id;
        Chain next;
    }
    public static void main(String[] args) {
        directed_algraph();
    }
    public static void directed_algraph() {
        int _numNodes;
        System.out.println("请输入节点个数");
        Scanner sc = new Scanner(System.in);
        _numNodes = sc.nextInt();
        Vexs[] vexs = new Vexs[_numNodes];
        System.out.println("请输入各个顶点的值");
        for(int i=0;i<vexs.length;i++) {
            sc = new Scanner(System.in);
            String temp = sc.nextLine();
            Vexs v = new ALGraph().new Vexs();
            v.data = temp;
            vexs[i] = v;
        }
        System.out.println("请输入各个顶点的出度");
        for(int i=0;i<vexs.length;i++) {
            System.out.println(vexs[i].data+"的出度:");
            sc = new Scanner(System.in);
            int temp = sc.nextInt();
            Chain head_chain = null;
            Chain current_chain = null;
            for(int j=0;j<temp;j++) {
                System.out.println("请输入"+vexs[i].data+"的邻接点的数组下标：");
                sc = new Scanner(System.in);
                if(head_chain==null) {
                    head_chain = new ALGraph().new Chain();
                    head_chain.id = sc.nextInt();
                    vexs[i].chain = head_chain;
                }else {
                    current_chain = new ALGraph().new Chain();
                    current_chain.id = sc.nextInt();
                    head_chain.next = current_chain;
                    head_chain = current_chain;
                }
            }
        }
        for(int i=0;i<vexs.length;i++) {
            int num = 0;
            while(vexs[i].chain != null) {
                if(num==0) {
                    System.out.print("顶点的值"+vexs[i].data+"的邻接点为:"+vexs[i].chain.id);
                    num++;
                }
                else {
                    System.out.print(","+vexs[i].chain.id);
                }
                vexs[i].chain = vexs[i].chain.next;
            }
            System.out.println();
        }
    }
}
