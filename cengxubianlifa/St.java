import java.util.LinkedList;
import java.util.Queue;
class Node<T> {
    T name;
    Node left;
    Node right;
    public Node() {
    }
    public Node(T name) {
        this.name = name;
    }
    public Node(T name, Node left, Node right) {
        this.name = name;
        this.left = left;
        this.right = right;
    }
}
public class BinaryTree {
    private static char[] a = {'A', 'B', '#', 'C', 'D', '#', '#', '#', 'E', '#', 'F', '#', '#'};
    private static int i = 0;
    public static Node Create() {
        if (i>= a.length || a[i]=='#') {
            i++;
            return null;
        }
        Node node = new Node<>(a[i+1]);
        node.left = Create();
        node.right = Create();
        return node;
    }
    public static void levOrder(Node root) {
        if (root != null) {
            Node linkedList = root;
            Queue<Node> queue = new LinkedList<>();
            queue.add(linkedList);
            while (!queue.isEmpty()) {
                linkedList= queue.poll();
                System.out.print(linkedList.name + " ");
                if (linkedList.left != null) {
                    queue.add(linkedList.left);
                }
                if (linkedList.right != null) {
                    queue.add(linkedList.right);
                }
            }
        }
    }
    public static void main(String[] args) {
        Node root = Create();
        System.out.println("\n层次遍历：");
        levOrder(root);
    }
}