public class LinkedStack1<T> implements Stack1<T>
{
    private final int DEFAULT=100;
    private Node top;
    private int count;
    private  T[] stack;
    @Override
    public T pop() {
        if (this.isEmpty()) {
            T result = (T) this.top.getElement();
            this.top = this.top.getNext();
            this.count--;
            return result;
        } else {
            T result = (T) this.top.getElement();
            this.top = this.top.getNext();
            this.count--;
            return result;
        }
    }
    @Override
    public void push(T element) {
        Node<T> temp = new Node(element);
        temp.setNext(this.top);
        this.top = temp;
        this.count++;
    }
    @Override
    public boolean isEmpty() {
        return this.count == 0;
    }
    @Override
    public int size() {
        return count;
    }
    @Override
    public String toString() {
        String result = "";
        System.out.println("�����\n");
        for(Node current=this.top; current != null; current = current.getNext()) {
            result = result + current.getElement().toString()+"\t";
        }
        return result;
    }
}

