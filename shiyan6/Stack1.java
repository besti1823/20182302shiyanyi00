public interface Stack1<T> {
    public void push(T element);
    public T pop();
    public boolean isEmpty();
    public int size();
    public String toString();
}
