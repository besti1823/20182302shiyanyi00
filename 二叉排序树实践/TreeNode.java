public class TreeNode {
    public TreeNode left;
    public TreeNode right;
    public int Element;
    public TreeNode(int element){
        left=null;
        right=null;
        Element=element;
    }
    public void setElement(int element){
        Element=element;
    }
    public int getElement(){
        return  Element;
    }
}