public class LinkedStack<T> {
    private int size;
    private Node temp;
    public LinkedStack(){
        temp = null;
    }
    public LinkedStack(T element){
        temp= new Node(element,null);
        size++;
    }
    public void push(T element){
        temp = new Node(element,temp);
        size++;
    }
    public T pop(){
        Node oldNode = temp;
        temp =temp.next;
        oldNode.next = null;
        size--;
        return oldNode.data;
    }
    public T peek(){
        return temp.data;
    }
    public boolean isEmpty(){
        return size == 0;
    }
    public int size(){
        return size;
    }
    public void geshihua() {
        size = 0;
        temp= null;
    }
    public String toString(){
        if(size == 0){
            return "[]";
        }
        StringBuilder b = new StringBuilder();
        for(Node current = temp;current != null;current = current.next){
            if(current.data != null){
                b.append(current.data.toString()).append(",");
            }else{
                b.append("null").append(',');
            }
        }
        int length = b.length();
        b.delete(length-1,length);
        return b.toString();
    }
    private class Node{
        private T data;
        private Node next;
        protected Node(T element, Node next) {
            this.data = element;
            this.next = next;
        }
    }
}