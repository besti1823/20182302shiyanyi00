import java.util.Arrays;
public class BinarySearch {
        public static
        boolean binarySearch(int[] data, int min, int max, int target)
        {
            boolean found = false;
            int mid = (min + max) / 2;
            if (data[mid]==target)
                found = true;
            else if (data[mid]>target)
            {
                if (min <= mid- 1)
                    found = binarySearch(data,min,mid-1, target);
            }

            else if (mid + 1 <= max)
                found = binarySearch(data, mid+1, max, target);

            return found;
        }

    }

