import java.util.Scanner;

public class newjisuan {
    public static void main(String[] args) {
        int a, num1, num2, num3, num4;
        double num5, num6, num7, num8;
        char char1, char2;
        RationalNumber r3 = null;
        Complex r4 = null;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("进行有理数计算请输入1，进行复数计算请输入0");
            a = scan.nextInt();
            if (a == 1) {
                System.out.println("请输入第一个有理数（依次输入分子分母即可）");
                num1 = scan.nextInt();
                num2 = scan.nextInt();
                System.out.println("请输入第二个有理数（依次输入分子分母即可）");
                num3 = scan.nextInt();
                num4 = scan.nextInt();
                RationalNumber r1 = new RationalNumber(num1, num2);
                RationalNumber r2 = new RationalNumber(num3, num4);
                System.out.println("第一个有理数: " + r1);
                System.out.println("第二个有理数 " + r2);
                System.out.println("请输如要运算的符号：");
                char1 = scan.next().charAt(0);
                switch (char1) {
                    case '+':
                        r3 = r1.add(r2);
                        break;
                    case '-':
                        r3 = r1.subtract(r2);
                        break;
                    case '*':
                        r3 = r1.multiply(r2);
                        break;
                    case '/':
                        r3 = r1.divide(r2);
                        break;
                    default:
                        break;
                }
                System.out.println("r1" + char1 + "r2 = " + r3);

            } else if (a == 0) {
                System.out.println("请输入第一个复数（依次输入实部虚部即可）");
                num5 = scan.nextDouble();
                num6 = scan.nextDouble();
                System.out.println("请输入第二个复数（依次输入实部虚部即可）");
                num7 = scan.nextDouble();
                num8 = scan.nextDouble();
                Complex r5 = new Complex(num5, num6);
                Complex r6 = new Complex(num7, num8);
                System.out.println("第一个复数: " + r5);
                System.out.println("第二个复数： " + r6);
                System.out.println("请输如要运算的符号：");
                char1 = scan.next().charAt(0);
                switch (char1) {
                    case '+':
                        r4 = r5.ComplexAdd(r6);
                        break;
                    case '-':
                        r4 = r5.ComplexSub(r6);
                        break;
                    case '*':
                        r4 = r5.ComplexMulti(r6);
                        break;
                    case '/':
                        r4 = r5.ComplexDiv(r6);
                        break;
                    default:
                        break;
                }
                System.out.println("r5" + char1 + "r6 = " + r4);

            }
            System.out.println("还要继续进行计算吗(y/n)?");
            char2 = scan.next().charAt(0);
        } while (char2 == 'y');
    }
}