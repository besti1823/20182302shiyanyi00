import java.util.Scanner;
public class CreatingExeception {
    public static void main(String[] args) throws StringToLongException {
        final int MAX = 20;
        Scanner scan =new Scanner(System.in);
        StringToLongException problem =new StringToLongException("Input string is out of range");
        System.out.print("Enter string length lower than MAX:");
        String  value=scan.nextLine();
        int num=value.length();
        if (num-4>MAX)
            throw problem;
        System.out.println("End of main method");
    }
}
