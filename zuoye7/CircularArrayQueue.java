public class CircularArrayQueue<T> implements Queue<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;
    public CircularArrayQueue (int initialCapacity)
    {
        front = rear = count = 0;
        queue = (T[]) (new Object[initialCapacity]);
    }
    public CircularArrayQueue()
    {
        this(DEFAULT_CAPACITY);
    }
    public void enqueue(T element)
    {
        if (size() == queue.length)
            expandCapacity();
        queue[rear] = element;
        rear = (rear+1) % queue.length;
        count++;
    }
    private void expandCapacity()
    {
        T[] larger = (T[]) (new Object[queue.length *2]);

        for (int scan = 0; scan < count; scan++)
        {
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }

        front = 0;
        rear = count;
        queue = larger;
    }
    public T dequeue()
    {
        if (isEmpty())
            System.out.println("queue");
        T result = queue[front];
        queue[front] = null;
        front = (front+1) % queue.length;
        count--;
        return result;
    }
    public T first()
    {
        T result = queue[front];
        return result;
    }
    public boolean isEmpty()
    {
        return front == rear;
    }
    public int size() {
        return count;
    }
    public String toString()
    {
        String result ="";
        int temp = front;
        for(int i = 0;i <= count; i++) {
            result += queue[i] + " ";
        }
        return result;
    }
}