import java.util.Stack;
import java.util.StringTokenizer;

public class zhuanhuan {
    private Stack<String> stack1;
    private Stack<String> stack2;
    private String input;
    private String result;
    public zhuanhuan(String input) {
        this.input = input;
        stack1= new Stack<>();
        stack2= new Stack<>();
    }
    public String doString() {
        StringTokenizer stn = new StringTokenizer(input, " ");
        while (stn.hasMoreElements()) {
            String temp = stn.nextToken();
            stack1.push(temp);
        }
        while (!stack1.empty()){
            String temp1=stack1.pop();
            char temp2=temp1.charAt(0);
            if ((temp2<'0'||temp2>'9')&&temp1.length()==1){
                String num2=stack2.pop();
                String num1=stack2.pop();
                result=num1+num2+temp2;
                stack2.push(result);
            }
            else {
                stack2.push(temp1);
            }
        }
        return result;
    }
}