public class CircularArrayQueueTest {
    public static void main(String[] args) {
        CircularArrayQueue queue = new CircularArrayQueue();
        System.out.println("Is it empty?");
        System.out.println(queue.isEmpty()+"\n\nrudui\n");
        queue.enqueue("2");
        queue.enqueue("0");
        queue.enqueue("1");
        queue.enqueue("8");
        queue.enqueue("2");
        queue.enqueue("3");
        queue.enqueue("0");
        queue.enqueue("2");
        System.out.println("Is it empty?");
        System.out.println(queue.isEmpty());
        System.out.println("string:"+queue.toString());
        System.out.println("first:"+queue.first());
        queue.dequeue();
        System.out.println("\nchudui\n\nstring:"+queue.toString());
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        System.out.println("\nchudui\n\nstring:");
        System.out.println("Is it empty?");
        System.out.println(queue.isEmpty());
    }
}