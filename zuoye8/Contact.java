public class Contact implements Comparable {
    protected String Firstname, Secondname, phone;
    public Contact(String S, String J, String W) {
        Firstname = S;
        Secondname = J;
        phone = W;
    }
    public String toString() {
        return Secondname +"  "+Firstname + ": " + phone;
    }
    @Override
    public int compareTo(Object o) {
        int result;
        result=phone.compareTo(((Contact) o).phone);
        return result;
    }
}