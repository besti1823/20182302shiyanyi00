import java.util.*;
import java.util.Queue;
import java.util.Scanner;
public class Tree1 {
    static int[] w;
    private static int maxLevel = 0;
    public class BinarayTreeNode {
        public int Value;
        public BinarayTreeNode LeftTreeNode = null;
        public BinarayTreeNode RigtTreeNode = null;
    }
    public BinarayTreeNode Construct(int[] preorder, int[] inorder) {
        if (preorder == null || inorder == null || inorder.length == 0 || preorder.length == 0) {
            return null;
        }
        int ptr = 0;
        int rootValue = preorder[0];
        BinarayTreeNode rootNode = new BinarayTreeNode();
        rootNode.Value = rootValue;
        while (ptr <= inorder.length && inorder[ptr] != rootValue) {
            ptr++;
        }
        w= new int[preorder.length - 1];

        for (int i = 0; i < preorder.length; i++) {
            if (i == 0) {
                continue;
            }
            w[i - 1] = preorder[i];
        }
        int[] inStart = new int[ptr];
        int[] inEnd = new int[inorder.length - ptr - 1];
        int ps = 0;
        for (int j = 0; j < inorder.length; j++) {
            if (j < ptr) {
                inStart[j] = inorder[j];
            }
            if (j > ptr) {
                inEnd[ps] = inorder[j];
                ps++;
            }
        }
        rootNode.LeftTreeNode = Construct(w, inStart);
        rootNode.RigtTreeNode = Construct(w, inEnd);
        return rootNode;
    }
    public static void PrintLevelTree(BinarayTreeNode pRoot) {
        Queue<BinarayTreeNode> nodeQueue = new LinkedList<BinarayTreeNode>();
        nodeQueue.add(pRoot);
        while (nodeQueue != null) {
            BinarayTreeNode node = nodeQueue.poll();
            if (node != null)
                System.out.print(node.Value);
            if (node != null && node.LeftTreeNode != null)
                nodeQueue.add(node.LeftTreeNode);
            if (node != null && node.RigtTreeNode != null)
                nodeQueue.add(node.RigtTreeNode);
            if (node == null) {
                System.out.println();
                break;
            }
        }
    }
    private static int getTreeLeverl(BinarayTreeNode pRoot, int level) {
        if (pRoot != null) {
            level++;
        }
        if( maxLevel < level ) maxLevel = level;
        if (pRoot.LeftTreeNode != null) {
            getTreeLeverl(pRoot.LeftTreeNode, level);
        }
        if (pRoot.RigtTreeNode != null) {
            getTreeLeverl(pRoot.RigtTreeNode, level);
        }
        return maxLevel ;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("�������������У���1,2,4,9,7,0,8,3,5��");
        String str = sc.next().toString();
        String[] arr  = str.split(",");
        int[] preorder = new int[arr.length];
        for(int j = 0; j<preorder.length;j++) {
            preorder[j] = Integer.parseInt(arr[j]);
        }
        System.out.println("�������������У���9,4,7,8,0,2,1,5,3��");
        String st = sc.next().toString();
        String[] ar  = st.split(",");
        int[] inorder = new int[ar.length];
        for(int j = 0; j<inorder.length;j++) {
            inorder[j] = Integer.parseInt(ar[j]);
        }
        BinarayTreeNode root = new Tree1().Construct(preorder, inorder);
        int totollevel = getTreeLeverl(root, 0);
        System.out.print("�������Ϊ\n");
        PrintLevelTree(root);
        Map<Integer, String> printInfo = new HashMap<Integer, String>();

    }
}